import { IContextUser } from "../resolvers/userResolver";


const loggedIn = (context: IContextUser) => {

    if (!context.user) {
        throw new Error("You are not logged in!");
    }
    
}

export default loggedIn;