import { BaseEntity, Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { ResourceEntity } from "./ResourceEntity";
import { UserEntity } from "./UserEntity";

@Entity({name: "prr"})
export class PrrEntity extends BaseEntity {

    @PrimaryGeneratedColumn("uuid")
    id: string | number;

    @Column()
    accountName: string;

    @Column()
    accountOwner: string;

    @Column()
    projectName: string;

    @Column()
    projectDescription: string;

    @Column()
    stage: string;

    @Column()
    probability: number;

    @CreateDateColumn()
    dateSubmitted: Date;

    @Column({nullable: true})
    resolutionStatus: string;

    @Column({default: "Submitted"})
    stateStatus: string;

    @ManyToOne(() => UserEntity, submitter => submitter.prrsSubmitted, {nullable: false})
    submitter: UserEntity;

    @ManyToOne(() => UserEntity, reviewer => reviewer.prrsReviewed, {nullable: true})
    reviewer: UserEntity;

    @OneToMany(() => ResourceEntity, resources => resources.prr)
    resources: ResourceEntity[];

}