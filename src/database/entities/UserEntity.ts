import { BaseEntity, Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { PrrEntity } from "./PrrEntity";

@Entity({name: "user"})
export class UserEntity extends BaseEntity {

    @PrimaryGeneratedColumn("uuid")
    id: string | number;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column({unique: true})
    email: string;

    @Column()
    serviceLine: string;

    @Column()
    squadLead: string;

    @Column()
    role: string;

    @CreateDateColumn()
    dateAdded: Date;

    @Column()
    password: string;

    @OneToMany(() => PrrEntity, prr => prr.submitter)
    prrsSubmitted: PrrEntity[];

    @OneToMany(() => PrrEntity, prr => prr.reviewer)
    prrsReviewed: PrrEntity[];

}
