import { BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { PrrEntity } from "./PrrEntity";

@Entity({name: "resource"})
export class ResourceEntity extends BaseEntity {

    @PrimaryGeneratedColumn("uuid")
    id: string | number;

    @Column()
    resourceLocation: string;

    @Column()
    projectRole: string;

    @Column()
    internalRole: string;

    @Column()
    serviceLine: string;

    @Column()
    skillSet: string;

    @Column()
    startDate: Date;

    @Column()
    endDate: Date;

    @Column()
    allocation: number;

    @Column({nullable: true})
    exampleProfile: string;

    @ManyToOne(() => PrrEntity, prr => prr.resources, {nullable: false})
    prr: PrrEntity;

}