import {MigrationInterface, QueryRunner} from "typeorm";

export class AddEnumsAsEntities1630063446199 implements MigrationInterface {
    name = 'AddEnumsAsEntities1630063446199'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "internalRoleEnum" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, CONSTRAINT "PK_f8f77c4e8400a1e1a85a006311c" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "resolutionStatusEnum" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, CONSTRAINT "PK_b70a67eddcf3b4d262781cdd9b6" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "roleEnum" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, CONSTRAINT "PK_19bc843a9b5f804a6cc9b6d65ec" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "serviceLineEnum" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, CONSTRAINT "PK_a6bfb36ad127996d7266473b76d" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "public"."resource" DROP COLUMN "internalRole"`);
        await queryRunner.query(`DROP TYPE "public"."resource_internalrole_enum"`);
        await queryRunner.query(`ALTER TABLE "public"."resource" ADD "internalRole" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."resource" DROP COLUMN "serviceLine"`);
        await queryRunner.query(`DROP TYPE "public"."resource_serviceline_enum"`);
        await queryRunner.query(`ALTER TABLE "public"."resource" ADD "serviceLine" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."user" DROP COLUMN "serviceLine"`);
        await queryRunner.query(`DROP TYPE "public"."user_serviceline_enum"`);
        await queryRunner.query(`ALTER TABLE "public"."user" ADD "serviceLine" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."user" DROP COLUMN "role"`);
        await queryRunner.query(`DROP TYPE "public"."user_role_enum"`);
        await queryRunner.query(`ALTER TABLE "public"."user" ADD "role" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."prr" DROP COLUMN "resolutionStatus"`);
        await queryRunner.query(`DROP TYPE "public"."prr_resolutionstatus_enum"`);
        await queryRunner.query(`ALTER TABLE "public"."prr" ADD "resolutionStatus" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."prr" DROP COLUMN "stateStatus"`);
        await queryRunner.query(`DROP TYPE "public"."prr_statestatus_enum"`);
        await queryRunner.query(`ALTER TABLE "public"."prr" ADD "stateStatus" character varying NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."prr" DROP COLUMN "stateStatus"`);
        await queryRunner.query(`CREATE TYPE "public"."prr_statestatus_enum" AS ENUM('submitted', 'reviewed')`);
        await queryRunner.query(`ALTER TABLE "public"."prr" ADD "stateStatus" "public"."prr_statestatus_enum" NOT NULL DEFAULT 'submitted'`);
        await queryRunner.query(`ALTER TABLE "public"."prr" DROP COLUMN "resolutionStatus"`);
        await queryRunner.query(`CREATE TYPE "public"."prr_resolutionstatus_enum" AS ENUM('accepted', 'rejected')`);
        await queryRunner.query(`ALTER TABLE "public"."prr" ADD "resolutionStatus" "public"."prr_resolutionstatus_enum"`);
        await queryRunner.query(`ALTER TABLE "public"."user" DROP COLUMN "role"`);
        await queryRunner.query(`CREATE TYPE "public"."user_role_enum" AS ENUM('seller', 'admin')`);
        await queryRunner.query(`ALTER TABLE "public"."user" ADD "role" "public"."user_role_enum" NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."user" DROP COLUMN "serviceLine"`);
        await queryRunner.query(`CREATE TYPE "public"."user_serviceline_enum" AS ENUM('ADD', 'MS', 'DA')`);
        await queryRunner.query(`ALTER TABLE "public"."user" ADD "serviceLine" "public"."user_serviceline_enum" NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."resource" DROP COLUMN "serviceLine"`);
        await queryRunner.query(`CREATE TYPE "public"."resource_serviceline_enum" AS ENUM('ADD', 'MS', 'DA')`);
        await queryRunner.query(`ALTER TABLE "public"."resource" ADD "serviceLine" "public"."resource_serviceline_enum" NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."resource" DROP COLUMN "internalRole"`);
        await queryRunner.query(`CREATE TYPE "public"."resource_internalrole_enum" AS ENUM('associate', 'associateEngineer', 'engineer', 'seniorEnginner', 'principalEngineer')`);
        await queryRunner.query(`ALTER TABLE "public"."resource" ADD "internalRole" "public"."resource_internalrole_enum" NOT NULL`);
        await queryRunner.query(`DROP TABLE "serviceLineEnum"`);
        await queryRunner.query(`DROP TABLE "roleEnum"`);
        await queryRunner.query(`DROP TABLE "resolutionStatusEnum"`);
        await queryRunner.query(`DROP TABLE "internalRoleEnum"`);
    }

}
