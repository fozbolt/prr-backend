import {MigrationInterface, QueryRunner} from "typeorm";

export class InitialMigration1629809904480 implements MigrationInterface {
    name = 'InitialMigration1629809904480'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "resource" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "resourceLocation" character varying NOT NULL, "projectRole" character varying NOT NULL, "internalRole" character varying NOT NULL, "serviceArea" character varying NOT NULL, "skillSet" character varying NOT NULL, "startDate" TIMESTAMP NOT NULL, "endDate" TIMESTAMP NOT NULL, "allocation" integer NOT NULL, "exampleProfile" character varying, CONSTRAINT "PK_e2894a5867e06ae2e8889f1173f" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "user" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "firstName" character varying NOT NULL, "lastName" character varying NOT NULL, "email" character varying NOT NULL, "serviceLine" character varying NOT NULL, "squadLead" character varying NOT NULL, "role" character varying NOT NULL, "dateAdded" TIMESTAMP NOT NULL DEFAULT now(), "password" character varying NOT NULL, CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "prr" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "accountName" character varying NOT NULL, "accountOwner" character varying NOT NULL, "projectName" character varying NOT NULL, "projectDescription" character varying NOT NULL, "stageProbability" character varying NOT NULL, "dateSubmitted" TIMESTAMP NOT NULL DEFAULT now(), "status" character varying NOT NULL, "sellerId" uuid, CONSTRAINT "PK_5907b2ebac99f72cfc444cf90ee" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "prr_resources_resource" ("prrId" uuid NOT NULL, "resourceId" uuid NOT NULL, CONSTRAINT "PK_08dccc040564576644a06a538c5" PRIMARY KEY ("prrId", "resourceId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_8c7243059e039acb056c66220d" ON "prr_resources_resource" ("prrId") `);
        await queryRunner.query(`CREATE INDEX "IDX_941ed3911ec9d83fd9228efb0b" ON "prr_resources_resource" ("resourceId") `);
        await queryRunner.query(`ALTER TABLE "prr" ADD CONSTRAINT "FK_85abcef0e8eb4e16b6767ada365" FOREIGN KEY ("sellerId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "prr_resources_resource" ADD CONSTRAINT "FK_8c7243059e039acb056c66220d4" FOREIGN KEY ("prrId") REFERENCES "prr"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "prr_resources_resource" ADD CONSTRAINT "FK_941ed3911ec9d83fd9228efb0bf" FOREIGN KEY ("resourceId") REFERENCES "resource"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "prr_resources_resource" DROP CONSTRAINT "FK_941ed3911ec9d83fd9228efb0bf"`);
        await queryRunner.query(`ALTER TABLE "prr_resources_resource" DROP CONSTRAINT "FK_8c7243059e039acb056c66220d4"`);
        await queryRunner.query(`ALTER TABLE "prr" DROP CONSTRAINT "FK_85abcef0e8eb4e16b6767ada365"`);
        await queryRunner.query(`DROP INDEX "IDX_941ed3911ec9d83fd9228efb0b"`);
        await queryRunner.query(`DROP INDEX "IDX_8c7243059e039acb056c66220d"`);
        await queryRunner.query(`DROP TABLE "prr_resources_resource"`);
        await queryRunner.query(`DROP TABLE "prr"`);
        await queryRunner.query(`DROP TABLE "user"`);
        await queryRunner.query(`DROP TABLE "resource"`);
    }

}
