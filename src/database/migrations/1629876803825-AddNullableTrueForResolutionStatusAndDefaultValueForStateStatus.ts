import {MigrationInterface, QueryRunner} from "typeorm";

export class AddNullableTrueForResolutionStatusAndDefaultValueForStateStatus1629876803825 implements MigrationInterface {
    name = 'AddNullableTrueForResolutionStatusAndDefaultValueForStateStatus1629876803825'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."prr" ALTER COLUMN "resolutionStatus" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."prr" ALTER COLUMN "stateStatus" SET DEFAULT 'submitted'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."prr" ALTER COLUMN "stateStatus" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "public"."prr" ALTER COLUMN "resolutionStatus" SET NOT NULL`);
    }

}
