import { getRepository } from "typeorm";
import { PrrEntity } from "../database/entities/PrrEntity";
import { UserEntity } from "../database/entities/UserEntity";
import loggedIn from "../middleware/loggedIn";
import { ICreateUserInput, IDeleteUserInput, IGetUserInput, ILoginUserInput, IUpdateUserInput } from "../schema/userSchema";
import prrResolver from "./prrResolver";

const bcrypt = require("bcrypt-nodejs");
const jwt = require('jsonwebtoken');

export interface IContextUser {
    user: {
        id: number | string;
        email: string;
        role: string;
    }
}

const userResolver = {

    Query: {

        getLoggedUser: async (_: unknown, args: unknown, context: IContextUser) => {

            loggedIn(context);

            const userId: IGetUserInput = {
                filter: {
                    id: context.user.id,
                }
            }

            const user = await userResolver.Query.getUser("", userId, context);

            return user;
        },

        getUsers: async (_: unknown, args: unknown, context: IContextUser) => {

            loggedIn(context);

            if (context.user.role !== 'Admin') {
                throw new Error("You can't see all users!");
            }

            const users = await getRepository(UserEntity).find();

            return users;
        },

        getUser: async (_: unknown, args: IGetUserInput, context: IContextUser) => {

            loggedIn(context);

            const user = await getRepository(UserEntity).findOneOrFail({
                where: {
                    id: args.filter.id
                }
            }).catch(() => {
                throw new Error("User not found!");
            });

            return user;
        }
    },

    Mutation: {

        loginUser: async (_: unknown, args: ILoginUserInput) => {

            const user = await getRepository(UserEntity).findOneOrFail({
                where: {
                    email: args.input.email,
                }
            }).catch(() => {
                throw new Error("User not found!");
            });

            const valid = bcrypt.compareSync(args.input.password, user.password);

            if (!valid) {
                throw new Error("Wrong password!");
            }

            const token = jwt.sign({
                id: user.id,
                email: user.email,
                role: user.role
            }, process.env.TOKEN_KEY);

            return {
                token
            };
        },

        createUser: async (_: unknown, args: ICreateUserInput, context: IContextUser) => {

            loggedIn(context);

            if (context.user.role !== 'Admin') {
                throw new Error("You can't create user!");
            }

            const hash = bcrypt.hashSync(args.input.password);

            const newUser = getRepository(UserEntity).create({
                firstName: args.input.firstName,
                lastName: args.input.lastName,
                email: args.input.email,
                password: hash,
                serviceLine: args.input.serviceLine,
                squadLead: args.input.squadLead,
                role: args.input.role,
            });
            await newUser.save();
            
            return newUser;
        },

        updateUser: async (_: unknown, args: IUpdateUserInput, context: IContextUser) => {

            loggedIn(context);

            if (context.user.role !== 'Admin') {
                throw new Error("You can't update user!");
            }

            if (!args.input.firstName && !args.input.lastName && !args.input.email && !args.input.password && !args.input.serviceLine && !args.input.squadLead && !args.input.role) {
                throw new Error("You must specify at least one property to update user's information!");
            }

            const user = await getRepository(UserEntity).findOneOrFail({
                where: {
                    id: args.input.id,
                }
            }).catch(() => {
                throw new Error("User not found!");
            });

            if (args.input.firstName) {
                user.firstName = args.input.firstName;
            }

            if (args.input.lastName) {
                user.lastName = args.input.lastName;
            }

            if (args.input.email) {
                user.email = args.input.email;
            }

            if (args.input.password) {
                const hash = bcrypt.hashSync(args.input.password);
                user.password = hash;
            }

            if (args.input.serviceLine) {
                user.serviceLine = args.input.serviceLine;
            }

            if (args.input.squadLead) {
                user.squadLead = args.input.squadLead;
            }

            if (args.input.role) {
                user.role = args.input.role;
            }

            const updatedUser = await user.save();

            return updatedUser;

        },

        deleteUser: async (_: unknown, args: IDeleteUserInput, context: IContextUser) => {

            loggedIn(context);

            if (context.user.role !== 'Admin') {
                throw new Error("You can't delete user!");
            }

            const prrsSubmitter = await prrResolver.Query.getPrrsSubmitter("", "", context);
            
            if (prrsSubmitter.length > 0) {
                throw new Error("You can't delete user if he or she is a submitter");
            }

            const userId: IGetUserInput = {
                filter: {
                    id: args.input.id,
                }
            }

            const user = await userResolver.Query.getUser("", userId, context);

            const prrsReviewer = await getRepository(PrrEntity).find({
                where: {
                    reviewer: user,
                }
            });

            if (prrsReviewer.length > 0) {
                throw new Error("You can't delete user if he or she is a reviewer");
            }
           
            const res = await getRepository(UserEntity).delete(args.input.id);

            if (res.affected && res.affected > 0) {
                return true;
            } else {
                return false;
            }
           
        },
    }

}

export default userResolver;