import { config } from "dotenv";
config();
import "reflect-metadata";
import cors from "cors"

import express from "express";
import corsConfig from "./config/corsConfig";
import { ApolloServer } from "apollo-server-express";
import connectToDatabase from "./config/databaseConfig";
import typeDefs from "./schema/schema";
import resolvers from "./resolvers/resolvers";
import editSchema from "./schema/editSchema";

const jwt = require('jsonwebtoken');

const main = async (): Promise<void> => {
  const app = express();

  await connectToDatabase();

  app.use(cors(corsConfig));

  // add enum values from database
  const newTypeDefs = await editSchema(typeDefs);

  const graphqlServer = new ApolloServer({
    typeDefs: newTypeDefs,
    resolvers: resolvers,
    context: async ({ req }) => {
      
        const token = req.headers.authorization || '';

        if (token) {
          const user = jwt.verify(token.substring(7), process.env.TOKEN_KEY);

        return {
          user
        };
        }
    }
  });

  await graphqlServer.start();

  graphqlServer.applyMiddleware({app, path: "/graphql"})

  app.listen(process.env.PORT, () => {
    console.log("App listening on", process.env.PORT, graphqlServer.graphqlPath);
  });
};

main();

