import { gql } from "apollo-server-express";

export interface IUser {
    id: string | number;
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    serviceLine: string;
    squadLead: string;
    role: string;
    dateAdded: Date;
}

export interface IGetUserInput {
    filter: {
        id: string | number;
    };
}

export interface ICreateUserInput {
    input: {
        firstName: string;
        lastName: string;
        email: string;
        password: string;
        serviceLine: string;
        squadLead: string;
        role: string;
    };
}

export interface IUpdateUserInput {
    input: {
        id: string | number;
        firstName?: string;
        lastName?: string;
        email?: string;
        password?: string;
        serviceLine?: string;
        squadLead?: string;
        role?: string;
    }
}

export interface IDeleteUserInput {
    input: {
        id: number | string;
    };
}

export interface ILoginUserInput {
    input: {
        email: string;
        password: string;
    };
}

const userSchema = gql`

    type User {
        id: ID!
        firstName: String!
        lastName: String!
        email: String!
        password: String!
        serviceLine: String!
        squadLead: String!
        role: String!
        dateAdded: Date!
    }

    input GetUserInput {
        id: ID!
    }

    type Query {
        getUsers: [User!]
        getUser(filter: GetUserInput!): User
        getLoggedUser: User
    }

    input CreateUserInput {
        firstName: String!
        lastName: String!
        email: String!
        password: String!
        serviceLine: ServiceLine!
        squadLead: String!
        role: UserRole!  
    }

    input UpdateUserInput {
        id: ID!
        firstName: String
        lastName: String
        email: String
        password: String
        serviceLine: ServiceLine
        squadLead: String
        role: UserRole
    }

    input DeleteUserInput {
        id: ID!
    }

    input LoginUserInput {
        email: String!
        password: String!
    }

    type Token {
        token: String!
    }

    type Mutation {
        createUser(input: CreateUserInput!): User!
        updateUser(input: UpdateUserInput!): User!
        deleteUser(input: DeleteUserInput!): Boolean!
        loginUser(input: LoginUserInput!): Token!
    }

`

export default userSchema;