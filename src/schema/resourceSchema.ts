import { gql } from "apollo-server-express";

export interface IResource {
    id: number | string;
    resourceLocation: string;
    projectRole: string;
    internalRole: string;
    serviceLine: string;
    skillSet: string;
    startDate: Date;
    endDate: Date;
    allocation: number;
    exampleProfile?: string;    
}

export interface IGetResourcesInput {
    filter: {
        prrId: number | string;
    };
}

export interface IGetResourceInput {
    filter: {
        id: number | string;
    };
}

export interface ICreateResourceInput {
    input: {
        prrId: number | string;
        resourceLocation: string;
        projectRole: string;
        internalRole: string;
        serviceLine: string;
        skillSet: string;
        startDate: Date;
        endDate: Date;
        allocation: number;
        exampleProfile?: string; 
    }
}

export interface IUpdateResourceInput {
    input: {
        id: number | string;
        resourceLocation?: string;
        projectRole?: string;
        internalRole?: string;
        serviceLine?: string;
        skillSet?: string;
        startDate?: Date;
        endDate?: Date;
        allocation?: number;
        exampleProfile?: string; 
    }
}

export interface IDeleteResourceInput {
    input: {
        id: number | string;
    };
}


const resourceSchema = gql`

    type Resource {
        id: ID!
        resourceLocation: String!
        projectRole: String!
        internalRole: String!
        serviceLine: String!
        skillSet: String!
        startDate: Date!
        endDate: Date!
        allocation: Int!
        exampleProfile: String
    }

    input GetResourcesInput {
        prrId: ID!
    }

    input GetResourceInput {
        id: ID!
    }

    type Query {
        getResources(filter: GetResourcesInput!): [Resource!]
        getResource(filter: GetResourceInput!): Resource
    }

    input CreateResourceInput {
        prrId: ID!
        resourceLocation: String!
        projectRole: String!
        internalRole: InternalRole!
        serviceLine: ServiceLine!
        skillSet: String!
        startDate: Date!
        endDate: Date!
        allocation: Number!
        exampleProfile: String
    }

    input UpdateResourceInput {
        id: ID!
        resourceLocation: String
        projectRole: String
        internalRole: InternalRole
        serviceLine: ServiceLine
        skillSet: String
        startDate: Date
        endDate: Date
        allocation: Number
        exampleProfile: String    
    }

    input DeleteResourceInput {
        id: ID!
    }

    type Mutation {
        createResource(input: CreateResourceInput!): Resource!
        updateResource(input: UpdateResourceInput!): Resource!
        deleteResource(input: DeleteResourceInput!): Boolean!
    }

`

export default resourceSchema;