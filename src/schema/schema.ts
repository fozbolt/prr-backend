import prrSchema from "./prrSchema";
import resourceSchema from "./resourceSchema";
import schemaScalars from "./schemaScalars";
import userSchema from "./userSchema";


const typeDefs = [userSchema, resourceSchema, prrSchema, schemaScalars];

export default typeDefs;