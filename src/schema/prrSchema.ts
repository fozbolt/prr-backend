import { gql } from "apollo-server-express";

export interface IPrr {
    id: number | string;
    accountName: string;
    accountOwner: string;
    projectName: string;
    projectDescription: string;
    stage: string;
    probability: number;
    dateSubmitted: Date;
    resolutionStatus: string;
    stateStatus: string;   
}

export interface IGetPrrInput {
    filter: {
        id: number | string;
    };
}

export interface IGetResourceForCreatePrrInput {
        resourceLocation: string;
        projectRole: string;
        internalRole: string;
        serviceLine: string;
        skillSet: string;
        startDate: Date;
        endDate: Date;
        allocation: number;
        exampleProfile?: string; 
}

export interface ICreatePrrInput {
    input: {
        accountName: string;
        accountOwner: string;
        projectName: string;
        projectDescription: string;
        stage: string;
        probability: number;
        resources: [IGetResourceForCreatePrrInput]
    }
}

export interface IUpdatePrrInput {
    input: {
        id: number | string;
        accountName?: string;
        accountOwner?: string;
        projectName?: string;
        projectDescription?: string;
        stage?: string;
        probability?: number;
        resolutionStatus?: string;
    }
}

export interface IDeletePrrInput {
    input: {
        id: number | string;
    }
}



const prrSchema = gql`

    type Prr {
        id: ID!
        accountName: String!
        accountOwner: String!
        projectName: String!
        projectDescription: String!
        stage: String!
        probability: Int!
        dateSubmitted: Date!
        resolutionStatus: String!
        stateStatus: String!
        resources: [Resource!]
    }

    input GetPrrInput {
        id: ID!
    }

    input GetPrrsSubmitterInput {
        id: ID!
    }

    type Query {
        getAllPrrs: [Prr!]
        getPrrsSubmitter: [Prr!]
        getPrr(filter: GetPrrInput!): Prr
        getReviewerForPrr(filter: GetPrrInput!): User
    }

    input GetResourceForCreatePrrInput {
        resourceLocation: String!
        projectRole: String!
        internalRole: InternalRole!
        serviceLine: ServiceLine!
        skillSet: String!
        startDate: Date!
        endDate: Date!
        allocation: Number!
        exampleProfile: String
    }

    input CreatePrrInput {
        accountName: String!
        accountOwner: String!
        projectName: String!
        projectDescription: String!
        stage: String!
        probability: Number!
        resources: [GetResourceForCreatePrrInput!]!
    }

    input UpdatePrrInput {
        id: ID!
        accountName: String
        accountOwner: String
        projectName: String
        projectDescription: String
        stage: String
        probability: Number
        resolutionStatus: ResolutionStatus
    }

    input DeletePrrInput {
        id: ID!
    }

    type Mutation {
        createPrr(input: CreatePrrInput!): Prr!
        updatePrr(input: UpdatePrrInput!): Prr!
        deletePrr(input: DeletePrrInput!): Boolean!
    }

`

export default prrSchema;