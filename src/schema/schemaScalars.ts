import { gql } from "apollo-server-express";


const schemaScalars = gql`
    scalar Date
    scalar Number
`

export default schemaScalars;