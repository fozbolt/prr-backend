import { gql } from "apollo-server-express";
import { GraphQLEnumType } from "graphql";
import { getRepository } from "typeorm";
import { InternalRoleEnumEntity } from "../database/entities/InternalRoleEnumEntity";
import { ResolutionStatusEnumEntity } from "../database/entities/ResolutionStatusEnumEntity";
import { RoleEnumEntity } from "../database/entities/RoleEnumEntity";
import { ServiceLineEnumEntity } from "../database/entities/ServiceLineEnumEntity";

const editSchema = async (typeDefs: any[]) => {

    // service lines
    // get all values from database for service line enum
    const serviceLinesRes = await getRepository(ServiceLineEnumEntity).find({
      select: [
        "name"
      ]
    });
  
    // create array of values
    let serviceLines: string[] = [];
    for (let i = 0; i < serviceLinesRes.length; ++i) {
      serviceLines.push(serviceLinesRes[i].name);
    }
  
    // create gql object for enum
    const serviceLinesTypeDef = gql`enum ServiceLine { ${serviceLines} }`;

    /*
    const servicelineEnum = new GraphQLEnumType( {
      name: 'Service Line',
      values: serviceLines
    })*/

    // roles
    const userRolesRes = await getRepository(RoleEnumEntity).find({
        select: [
            "name"
          ]
    });

    let userRoles: string[] = [];
    for (let i = 0; i < userRolesRes.length; ++i) {
        userRoles.push(userRolesRes[i].name);
    }

    const userRolesTypeDef = gql` enum UserRole { ${userRoles} }`;

    // internal roles
    const internalRolesRes = await getRepository(InternalRoleEnumEntity).find({
        select: [
            "name"
          ]
    });

    let internalRoles: string[] = [];
    for (let i = 0; i < internalRolesRes.length; ++i) {
        internalRoles.push(internalRolesRes[i].name);
    }

    const internalRolesTypeDef = gql` enum InternalRole { ${internalRoles} }`;

    // resolution statuses
    const resolutionStatusesRes = await getRepository(ResolutionStatusEnumEntity).find({
        select: [
            "name"
          ]
    });
    
    let resolutionStatuses: string[] = [];
    for (let i = 0; i < resolutionStatusesRes.length; ++i) {
        resolutionStatuses.push(resolutionStatusesRes[i].name);
    }

    const resolutionStatusesTypeDef = gql` enum ResolutionStatus { ${resolutionStatuses} }`;

    // add created gql objects to typeDefs array
    const newTypeDefs = [serviceLinesTypeDef, userRolesTypeDef, internalRolesTypeDef, resolutionStatusesTypeDef, typeDefs].flat();

    // console.log(JSON.stringify(newTypeDefs, null, 2));
 
    return newTypeDefs;
  }

export default editSchema;