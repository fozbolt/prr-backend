import { CorsOptions } from "cors"

const whitelist: string[] = [
    "https://localhost:" + process.env.PORT,
    "https://localhost:" + process.env.PORT + "/",
    "https://studio.apollographql.com",
    "http://localhost:3000",
]

const corsConfig: CorsOptions = {
    origin: function (origin, callback) {
     // if (whitelist.indexOf(origin) !== -1) {
         // do drugog uvjeta će doć samo ako je prosao prvi uvjet
        if (typeof origin === "undefined" || whitelist.includes(origin)) {
        callback(null, true)
      } else {
        callback(new Error('Not allowed by CORS'))
      }
    }
  }

export default corsConfig;