import { ConnectionOptions } from "typeorm";
import { InternalRoleEnumEntity } from "./src/database/entities/InternalRoleEnumEntity";
import { PrrEntity } from "./src/database/entities/PrrEntity";
import { ResolutionStatusEnumEntity } from "./src/database/entities/ResolutionStatusEnumEntity";
import { ResourceEntity } from "./src/database/entities/ResourceEntity";
import { RoleEnumEntity } from "./src/database/entities/RoleEnumEntity";
import { ServiceLineEnumEntity } from "./src/database/entities/ServiceLineEnumEntity";
import { UserEntity } from "./src/database/entities/UserEntity";
import { InitialMigration1629809904480 } from "./src/database/migrations/1629809904480-InitialMigration";
import { AddEnumForServiceLineAndUserRole1629810180366 } from "./src/database/migrations/1629810180366-AddEnumForServiceLineAndUserRole";
import { FixRelationsAndAddEnums1629820496872 } from "./src/database/migrations/1629820496872-FixRelationsAndAddEnums";
import { AddServiceLineEnumForResources1629821005081 } from "./src/database/migrations/1629821005081-AddServiceLineEnumForResources";
import { AddNullableFalseToResourceAndPrrEntities1629821430886 } from "./src/database/migrations/1629821430886-AddNullableFalseToResourceAndPrrEntities";
import { AddNullableTrueForResolutionStatusAndDefaultValueForStateStatus1629876803825 } from "./src/database/migrations/1629876803825-AddNullableTrueForResolutionStatusAndDefaultValueForStateStatus";
import { AddEnumsAsEntities1630063446199 } from "./src/database/migrations/1630063446199-AddEnumsAsEntities";
import { AddNullableAndDefaultPropertiesToPrrEntity1630066998863 } from "./src/database/migrations/1630066998863-AddNullableAndDefaultPropertiesToPrrEntity";
import { AddUniquePropertyToEmail1630418582915 } from "./src/database/migrations/1630418582915-AddUniquePropertyToEmail";

const dataBaseConfig: ConnectionOptions = {
type: "postgres",
host: process.env.DB_HOST,
port: Number(process.env.DB_PORT || 5432),
database: process.env.DB_DATABASE,
username: process.env.DB_USERNAME,
password: process.env.DB_PASSWORD,
logging: "all",
synchronize: false,
entities: [UserEntity, PrrEntity, ResourceEntity, RoleEnumEntity, ServiceLineEnumEntity, InternalRoleEnumEntity, ResolutionStatusEnumEntity],
migrations: [InitialMigration1629809904480, AddEnumForServiceLineAndUserRole1629810180366, FixRelationsAndAddEnums1629820496872, AddServiceLineEnumForResources1629821005081, AddNullableFalseToResourceAndPrrEntities1629821430886, AddNullableTrueForResolutionStatusAndDefaultValueForStateStatus1629876803825, AddEnumsAsEntities1630063446199, AddNullableAndDefaultPropertiesToPrrEntity1630066998863, AddUniquePropertyToEmail1630418582915],
cli: {
    entitiesDir: "src/database/entities",
    migrationsDir: "src/database/migrations",
},
};

export default dataBaseConfig;